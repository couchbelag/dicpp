#pragma once

#include "dependency_interface.h"

namespace example
{
    class Formal : public IDependency
    {
        public:
            Formal(const std::string name) {}

            virtual std::string Call() const
            {
                return "Sir";
            }

            virtual std::string Greet() const
            {
                return std::string("Dear ") + Call();
            }
    };
}
