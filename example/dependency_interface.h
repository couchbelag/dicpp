#pragma once

#include <string>

namespace example
{
    class IDependency
    {
        public:
            virtual ~IDependency() {}

            virtual std::string Call() const = 0;
            virtual std::string Greet() const = 0;
    };
}
