#include "example.h"
#include "di.h"
#include "dependency_interface.h"

namespace example
{
    Example::Example()
    {
        dependency = di::as<IDependency>::create(std::string("Dude"));
    }

    std::string Example::Greet() const
    {
        return dependency->Greet();
    }
}
