#pragma once

#include <string>

namespace example
{
    // forward declaration, will be resolved by dependecy injection
    class IDependency;


    class Example
    {
        public:
            Example();
            std::string Greet() const;

        private:
            IDependency* dependency;
    };
}
