#pragma once

#include "dependency_interface.h"

namespace example
{
    class Informal : public IDependency
    {
        public:
            Informal(const std::string name) : m_name(name) {}

            virtual std::string Call() const
            {
                return m_name;
            }

            virtual std::string Greet() const
            {
                return std::string("Hey ") + Call() + "!";
            }

        private:
            const std::string m_name;
    };
}
