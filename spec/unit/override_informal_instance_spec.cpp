#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"
#include "dependency_informal.h"
#include "dependency_formal.h"
#include "di.h"

using namespace example;

SCENARIO("As a developer I want to override informal default object", "[override]")
{
    WHEN("formal class is injected programmatically by id")
    {
        Informal* dep1 = new Informal("Dude");
        di::as<IDependency>::inject().none();
        di::as<IDependency>::inject(dep1);

        THEN("different IDs yield different objects")
        {
            example::IDependency* r1 = di::as<example::IDependency>::yield();
            REQUIRE(r1->Greet() == "Hey Dude!");
        }
    }
}
