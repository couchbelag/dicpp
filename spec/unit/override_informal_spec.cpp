#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"
#include "dependency_informal.h"
#include "di.h"


SCENARIO("As a developer I want to override informal default factory", "[override]")
{
    GIVEN("formal class is injected programmatically overriding informal defaults")
    {
        di::as<example::IDependency>::inject<example::Informal, const std::string>();

        WHEN("example is instantiated")
        {
            example::Example example;
            THEN("it greets in a formal voice")
            {
                REQUIRE(example.Greet() == "Hey Dude!");
            }
        }
    }
}
