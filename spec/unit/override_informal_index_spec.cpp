#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"
#include "dependency_informal.h"
#include "dependency_formal.h"
#include "di.h"

using namespace example;

SCENARIO("As a developer I want to override informal default object by tag", "[override]")
{
    WHEN("formal class is injected programmatically by id")
    {
        Informal* dep1 = new Informal("Dude");
        Formal* dep2 = new Formal("");
        di::as<IDependency>::inject<1>().none();
        di::as<IDependency>::inject<1>(dep1);
        di::as<IDependency>::inject<2>(dep2);

        THEN("different IDs yield different objects")
        {
            IDependency* r1 = di::as<IDependency>::yield<1>();
            REQUIRE(r1->Greet() == "Hey Dude!");

            IDependency* r2 = di::as<IDependency>::yield<2>();
            REQUIRE(r2->Greet() == "Dear Sir");
        }

        THEN("tags are searchable at runtime")
        {
            int mutableVar = 4;
            IDependency *r1, *r2;
            REQUIRE_NOTHROW(
                r1 = di::as<IDependency>::from<1>::to<5>::yield(mutableVar)
            );
            REQUIRE_NOTHROW(
                r2 = di::as<IDependency>::from<1>::to<5>::yield(5)
            );
            REQUIRE(r1 != nullptr);
            REQUIRE(r2 == nullptr);

            REQUIRE(di::as<IDependency>::present<4>());
            REQUIRE(!di::as<IDependency>::present<3>());
        }
    }
}
