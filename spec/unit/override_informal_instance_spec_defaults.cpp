#include "di.h"
#include "dependency_informal.h"

using namespace example;

// defaults for injection
template<>
template<>
di::as<IDependency>::configuration di::as<IDependency>::generic::configure{};
