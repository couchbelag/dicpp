#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"


SCENARIO("As a developer I want to set formal defaults", "[defaults]")
{
    GIVEN("an example")
    {
        example::Example example;

        WHEN("it is linked with 'formal' defaults")
        {
            THEN("it greets in a formal voice")
            {
                REQUIRE(example.Greet() == "Dear Sir");
            }
        }
    }
}
