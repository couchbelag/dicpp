#include "di.h"
#include "dependency_formal.h"

using namespace example;

// defaults for injection
template<>
template<>
di::as<IDependency>::configuration di::as<IDependency>::generic::configure{create_factory<Formal, const std::string>()};
