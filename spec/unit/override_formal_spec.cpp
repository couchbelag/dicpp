#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"
#include "dependency_formal.h"
#include "di.h"


SCENARIO("As a developer I want to override formal defaults", "[override]")
{
    GIVEN("informal class is injected programmatically overriding formal defaults")
    {
        di::as<example::IDependency>::inject<example::Formal, const std::string>();

        WHEN("example is instantiated")
        {
            example::Example example;
            THEN("it greets in a formal voice")
            {
                REQUIRE(example.Greet() == "Dear Sir");
            }
        }
    }
}
