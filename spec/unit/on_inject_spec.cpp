#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"
#include "dependency_informal.h"
#include "di.h"

using namespace example;

SCENARIO("As a developer I want to get notifie", "[override]")
{
    GIVEN("new objects are injected")
    {
        WHEN("injected into a generic container")
        {
            Informal* obj = new Informal("Sir");

            THEN("it notifies registered callbacks")
            {
                int counter = 0;
                di::as<IDependency>::on_inject(
                    [&](IDependency* dep, int) {
                        REQUIRE(dep == obj);
                        counter++;
                    }
                );

                di::as<IDependency>::on_inject(
                    [&](IDependency* dep, int) {
                        REQUIRE(dep == obj);
                        counter++;
                    }
                );

                di::as<IDependency>::inject(obj);
                REQUIRE(counter == 2);
            }
        }

        WHEN("injected into a tagged container")
        {
            Informal* obj = new Informal("Sir");

            THEN("it notifies registered callbacks")
            {
                int counter = 0;

                di::as<IDependency>::OnInjectTaggedCallback callback =
                    [&](IDependency* dep, int tag) {
                        REQUIRE(dep == obj);
                        counter += tag;
                    };

                di::CallbackHandle cbHandle = di::as<IDependency>::on_inject<3>(callback);

                di::as<IDependency>::on_inject<3>(
                    [&](IDependency* dep, int tag) {
                        counter += tag;
                    }
                );

                di::as<IDependency>::on_inject<4>(
                    [&](IDependency* dep, int tag) {
                        REQUIRE(dep == obj);
                        counter += tag;
                    }
                );

                di::as<example::IDependency>::inject<3>(obj);
                di::as<example::IDependency>::inject<4>(obj);
                REQUIRE(counter == 10);

                di::as<IDependency>::off_inject<3>(cbHandle);
                di::as<example::IDependency>::inject<3>(new Informal(""));
                REQUIRE(counter == 13);
            }
        }
    }
}
