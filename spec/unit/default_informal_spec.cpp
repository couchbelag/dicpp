#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "example.h"


SCENARIO("As a developer I want to set informal defaults", "[defaults]")
{
    GIVEN("an example")
    {
        example::Example example;

        WHEN("it is linked with 'informal' defaults")
        {
            THEN("it greets in an informal voice")
            {
                REQUIRE(example.Greet() == "Hey Dude!");
            }
        }
    }
}
