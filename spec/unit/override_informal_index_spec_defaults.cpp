#include "di.h"
#include "dependency_informal.h"

using namespace example;
using namespace di;

// defaults for injection
template<> template<> as<IDependency>::configuration as<IDependency>::tagged<1>::configure{};
template<> template<> as<IDependency>::configuration as<IDependency>::tagged<2>::configure{};
template<> template<> as<IDependency>::configuration as<IDependency>::tagged<3>::configure{};
template<> template<> as<IDependency>::configuration as<IDependency>::tagged<4>::configure{new Informal("Dude")};
template<> template<> as<IDependency>::configuration as<IDependency>::tagged<5>::configure{};
