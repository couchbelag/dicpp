# dicpp

*dicpp* is a single header only dependency injection implementation using C++ meta programming. This approach tries to move dependency injection implementation to compile time execution. Even defaults will not need to be set up at runtime.

Basically *dicpp* keeps track of tagged items of certain types which could be factories for that type or objects of that type (factories and object can exist in parallel). The *tags* are IDs which will resolve metaprogrammatically to actual objects or factories of the *interface type*. Of course the yielded objects' types of those items are presumably inherited from the *interface type*. This way it is possible to register multiple items per *interface type*.

Apart from those *tagged* items, there can also be more simple *generic* items. Those items are only identified bythe*interface type* to which they're registered and don't have a tag. *generic* items are thus limited to resolve onlytoone object or factory per *interface type*. Just a hint to the implementation of *generic* items, actually they dohavea tag (`di::NONE`) which is the minimum integer value. So don't use this value for explicitly tagging items.

# Pointers

... are solely in your responsibility, *dicpp* will **not** take care of deleting pointers when they're not used any more.


# API

For explanation of the API the following definitions are used throughout the examples:

```cpp
// list of custom tags
enum {
    MY_TAG_1,
    MY_TAG_2
};

// interface definition for use as dependency
class IMyInterface {};

// actual implementations to be injected
class MyClass : public IMyInterface {
    MyClass(const std::string params) {}
};

class AnotherClass : public IMyInterface {
    AnotherClass(const std::string constructor_params_should_be_the_same) {}
};
```


## Static configuration

Items are kept in static variables, that need to be allocated before they can be used. So you will need to provide a definition of those static initializations for every generic and tagged item you will be using at runtime. **If you don't the linker will complain about *undefined references*, so better keep an eye on the linker's output!** In case the error message mentions a tag with a hugely negative number... you're missing a *generic* item's initialization, have a look at the *interface type*.

Basically a configuration is set up using a `configuration` and `configure` it with objects and factories that suit your use case. Please use '{}' for initialization instead of '()', this will play more nicely with empty initialization.

```cpp
template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::generic::configure{ new MyClass("some") };
```
More detailed examples will follow in the subsequent chapters.


## Using generic factories

In this example we will create a factory building objects of type `MyClass`. When requested these objects will be returned as type `IMyInterface`.


```cpp
// initialization
template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::generic::configure{ create_factory<AnotherClass, const std::string>() };

void main() {
    // will overwrite initialization

    di::as<IMyInterface>::inject<MyClass, const std::string>();

    // ...

    // create object for interface type
    IMyInterface* obj = di::as<IMyInterface>::create("constructor parameter value");
}
```


## Using tagged factories

In this example we will create factories with tags building objects of type `MyClass`. When requested these objects will be returned according to their tags as type `IMyInterface`.

```cpp
// initialization for MY_TAG_1
template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::tagged<MY_TAG_1>::configure{
    create_factory<MyClass, const std::string>();
};

// initialization for MY_TAG_2
template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::tagged<MY_TAG_2>::configure{
    create_factory<AnotherClass, const std::string>();
};


void main() {
    // will overwrite initialization of MY_TAG_1 (but doesn't change a thing actually)
    di::as<IMyInterface>::inject<MyClass, MY_TAG_1, const std::string>();

    // ...

    // create different objects for same interface type
    IMyInterface* obj1 = di::as<IMyInterface>::create<MY_TAG_1>("constructor parameter value");
    IMyInterface* obj2 = di::as<IMyInterface>::create<MY_TAG_2>("constructor parameter value");
}
```


## Using generic objects

In this example we will provide objects of type `MyClass` to dependency containers. When request these objects will be returned by the dependency container as *interface type* `MyInterface`.


```cpp
// initialization with nullptr
template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::generic::configure{};

void main() {
    // will overwrite initialization
    di::as<IMyInterface>::inject(new MyClass("keep safe!"));

    // ...

    // retrieve object for interface type
    IMyInterface* obj = di::as<IMyInterface>::yield();

    // reset a generic object (set to nullptr)
    IMyInterface* obj2 = di::as<IMyInterface>::inject().none();
}
```


## Using tagged objects

In this example we will provide objects of type `MyClass` to dependency containers. When request these objects will be returned by the dependency container as *interface type* `MyInterface`.


```cpp
// initialization
template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::tagged<MY_TAG_1>::configure{};

template<>
template<>
di::as<IMyInterface>::configuration di::as<IMyInterface>::tagged<MY_TAG_2>::configure{
    new AnotherClass("be kind!"
};

void main() {
    // overwrite nullptr from initialization if not done yet
    if (!di::as<IMyInterface>::present<MY_TAG_1>())
    {
        di::as<IMyInterface>::inject<MY_TAG_1>(new MyClass("reuse, reduce, recycle!"));
    }

    // ...

    // retrieve objects for interface type according to tags
    IMyInterface* obj1 = di::as<IMyInterface>::yield<MY_TAG_1>();
    IMyInterface* obj2 = di::as<IMyInterface>::yield<MY_TAG_2>();

    // search for a specific tag in a range
    // useful for looking up tags dynamically from a variable value
    int tag = MY_TAG_2;
    IMyInterface* obj2 = di::as<IMyInterface>::from<MY_TAG_1>::to<MY_TAG_2>::yield(tag);

    // reset a tagged object (set to nullptr)
    IMyInterface* obj2 = di::as<IMyInterface>::inject<MY_TAG_2>().none();
}
```


## Using notifications

`dicpp` is able to provide notifications when changes are applied to objects in dependency containers. This is limited to changes of objects, factories are not supported, yet.

```cpp
// add notification for generic objects
di::as<IDependency>::on_inject(
    // e.g. use lambda to react on events
    [&](IDependency* dep, int tag) {
        counter += tag;
    }
);

// add notification for tagged objects
di::as<IDependency>::on_inject<3>(
    // e.g. use lambda to react on events
    [&](IDependency* dep, int tag) {
        counter += tag;
    }
);
```