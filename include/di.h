/*
 * MIT License
 *
 * Copyright (c) 2021 Tobi Schmid
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */



/**
 * This project is hosted in gitlab.com (https://gitlab.com/couchbelag/dicpp)
 */


#pragma once

#define DICPP_VERSION_MAJOR   0
#define DICPP_VERSION_MINOR   5
#define DICPP_VERSION_PATCH   2

/**
 * @brief enable/disable debugging information
 *
 * Enable (true) or disable (false) debugging information
 */
#define DICPP_TEMPLATE_DEBUG false

#if DICPP_TEMPLATE_DEBUG == true
    #include <iostream>
    #include <iomanip>
#endif

#include <limits>
#include <functional>
#include <map>

/**
 * @brief dependency injection
 */
namespace di
{
    #if DICPP_TEMPLATE_DEBUG == true
        /**
         * @brief helper to resolve argument typess recursively
         *
         * @tparam Args argument types
         */
        template<typename... Args>
        struct arg_types_resolver;

        /**
         * @brief helper to resolve argument typess recursively
         *
         * @tparam Args argument types
         */
        template<typename First, typename... Args>
        struct arg_types_resolver<First, Args...>
        {
            static std::string resolve()
            {
                return std::string(typeid(First).name()) + ", " + arg_types_resolver<Args...>::resolve();
            }
        };

        /**
         * @brief helper to resolve argument typess recursively
         *
         * this is the specialization of void argument list in functions,
         * and thus the breaking point for the recursive resolver
         */
        template<>
        struct arg_types_resolver<>
        {
            static std::string resolve()
            {
                return "";
            }
        };

        /**
         * @brief create string from provided arguments information
         *
         * this is the specialisation for the last argument as breaking point for the
         * variadic template recursion.
         *
         * @tparam First    argument type
         * @param first     argument value
         * @return std::string
         */
        template<typename First>
        std::string resolve_args(First first)
        {
            return std::string("'") + first + "'";
        }

        /**
         * @brief create string from provided arguments information
         *
         * this is the variadic template function to recurse through
         * the list of arguments.
         *
         * @tparam First    first argument type
         * @tparam Second   second argument types
         * @tparam Args     further argument types if available
         * @param first     first argument value
         * @param second    second argument value
         * @param args      further argument values if available
         * @return std::string
         */
        template<typename First, typename Second, typename... Args>
        std::string resolve_args(First first, Second second, Args... args)
        {
            return resolve_args(first) + resolve_args(second, args...);
        }
    #endif

    /**
     * @brief tag for not tagging an item ;-)
     *
     * This tag is used to mark the default item
     * that is yielded when no tag has been applied
     * to it. Don't use it's value
     * ( == std::numeric_limits<int>::min() )
     * in your own list of tags
     */
    enum {
        NONE = std::numeric_limits<int>::min()
    };


    /**
     * @brief type definition for factory functions
     */
    using Factory = void(*)();

    /**
     * @brief template for factory functions
     *
     * function template for creating factory functions. this is
     * not intended to be used directly. if you need to create
     * a factory (e.g. for default injections) use the template
     * function di::create_factory() instead.
     *
     * @tparam AType    actual class that is to be instantiated on injection
     * @tparam Args     actual class' constructor parameter list for constructor identification
     * @param args      arguments that will be passed to the constructor on injection
     * @return AType*   instance of the actual class
     */
    template<typename AType, typename... Args>
    AType* injector(Args... args)
    {
        #if DICPP_TEMPLATE_DEBUG  == true
            std::cout << "actual type name:     " << typeid(AType).name() << std::endl;
            std::cout << "argument type names:  " << arg_types_resolver<Args...>::resolve() << std::endl;
            std::cout << "argument values:      " << resolve_args(args...) << std::endl;
        #endif

        return new AType(args...);
    }

    /**
     * @brief Create a factory object
     *
     * This funtion can be used to create a default generic factory,
     * that will be linked to the application and will be used
     * when no runtime injections have been made so far.
     *
     * @tparam AType    actual class to be injected
     * @tparam Args     actual class' constructor parameter list
     * @return Factory
     */
    template<typename AType, typename... Args>
    constexpr static Factory create_factory()
    {
        /*
         * MSVC won't let me cast into void(*)(), so we need
         * the detour to void*...
         *
         * anybody ideas on getting this (type) safe?
         */
        void *ptr = reinterpret_cast<void*>(injector<AType, Args...>);
        return reinterpret_cast<Factory>(ptr);
    }

    using CallbackHandle = int;

    /**
     * @brief
     *
     * this is the container providing injection
     * information that is used upon injection
     * or dependency instantiation
     *
     * @tparam IType    interface type that will be requested as dependency
     */
    template<typename IType>
    struct as
    {
        using OnInjectTaggedCallback = std::function<void(IType*, int)>;
        static void OnInjectCallbackDefault(IType* obj, int tag) {}

        struct configuration
        {
            Factory factory;
            IType* object;
            std::map<int, OnInjectTaggedCallback> on_inject;
            CallbackHandle callback_counter;
            bool autodelete;


            configuration(Factory factory, IType* object, bool autodelete = false)
            {
                this->factory = factory;
                this->object = object;
                this->callback_counter = 0;
                this->autodelete = autodelete;
            }
            configuration(Factory factory, bool autodelete = false) : configuration(factory, nullptr, autodelete) {}
            configuration(IType* object, bool autodelete = false) : configuration(nullptr, object, autodelete) {}
            configuration(bool autodelete) : configuration(nullptr, nullptr, autodelete) {}
            configuration() : configuration(nullptr, nullptr, false) {}
        };

        /**
         * @brief register objects to look them up by tags
         *
         * @tparam TAG
         */
        template<int TAG>
        struct tagged
        {
            static configuration configure;

            tagged() {}

            void none()
            {
                inject(nullptr);
            }

            template<typename ActualType, typename... Args>
            static void inject()
            {
                configure.factory = di::create_factory<ActualType, Args...>();
            }

            static void inject(IType* obj)
            {
                IType* tmp = configure.object;
                configure.object = obj;
                notify();

                if (configure.autodelete && tmp)
                {
                    delete tmp;
                }
            }

            static IType* yield()
            {
                return configure.object;
            }

            template<typename... Args>
            static IType* create(Args... args)
            {
                return reinterpret_cast<IType*(*)(Args...)>(configure.factory)(args...);
            }

            static CallbackHandle on_inject(OnInjectTaggedCallback callback)
            {
                CallbackHandle handle = configure.callback_counter++;
                configure.on_inject[handle] = callback;
                return handle;
            }

            static void off_inject(CallbackHandle handle)
            {
                configure.on_inject.erase(handle);
            }

            static void notify()
            {
                for (std::pair<int, OnInjectTaggedCallback> callback : configure.on_inject)
                {
                    callback.second(configure.object, TAG);
                }
            }
        };

        using generic = tagged<NONE>;

        template<int TAG>
        static void inject(IType* obj)
        {
            tagged<TAG>::inject(obj);
        }

        template<int TAG>
        static tagged<TAG> inject()
        {
            return tagged<TAG>();
        }

        /**
         * @brief default instance returned for an interface
         */
        static void inject(IType* obj)
        {
            generic::inject(obj);
        }

        static generic inject()
        {
            return generic();
        }

        /**
         * @brief inject actual implementation for dependent interface
         *
         * this injects an actual implementation of IType template argument
         * into this container. any calls to as<IType>::create() will
         * instantiate this actual class
         *
         * @tparam ActualType   actual class to be injected
         * @tparam Args         actual class' constructor parameter types
         */
        template<typename ActualType, typename... Args>
        static void inject()
        {
            generic::template inject<ActualType, Args...>();
        }

        template<typename ActualType, int TAG, typename... Args>
        static void inject()
        {
            tagged<TAG>::template inject<ActualType, Args...>();
        }

        /**
         * @brief instantiate a dependency
         *
         * resolves a dependency, that is IType template argument,
         * to an actual implementation that has been injected before.
         *
         * @tparam Args     constructor parameter list types
         * @param args      constructor parameter list values
         * @return IType*   instance of an actual implementation
         */
        template<typename... Args>
        static IType* create(Args... args)
        {
            return generic::create(args...);
        }

        template<int TAG, typename... Args>
        static IType* create(Args... args)
        {
            return tagged<TAG>::create(args...);
        }

        template<int TAG>
        static IType* yield()
        {
            return tagged<TAG>::yield();
        }

        static IType* yield()
        {
            return generic::yield();
        }

        template<int TAG>
        static bool present()
        {
            return tagged<TAG>::yield() != nullptr;
        }

        static bool present()
        {
            return generic::yield() != nullptr;
        }

        template<int TAG>
        static CallbackHandle on_inject(OnInjectTaggedCallback callback)
        {
            return tagged<TAG>::on_inject(callback);
        }

        static CallbackHandle on_inject(OnInjectTaggedCallback callback)
        {
            return generic::on_inject(callback);
        }

        template<int TAG>
        static void off_inject(CallbackHandle handle)
        {
            tagged<TAG>::off_inject(handle);
        }

        static void off_inject(CallbackHandle handle)
        {
            generic::off_inject(handle);
        }

        template<int TAG>
        static void notify()
        {
            tagged<TAG>::notify();
        }

        static void notify()
        {
            generic::notify();
        }

        /**
         * @brief don't use this, take from<x>::to<y> structs instead
         *
         * this is a ranged yield implementation in order to
         * find a certain tag in a range. don't use this, take from::to
         * structs instead!
         *
         * @tparam FROM     first tag to start search
         * @tparam TO       last tag + 1 (one ahead of the tags list,
         *                  that's why you shouldn't use it, it presumably
         *                  will not be used correctly)
         */
        template<int FROM, int TO>
        struct range
        {
            constexpr static IType* yield(int tag)
            {
                if (FROM > tag || TO < tag)
                {
                    throw "requested tag is out of range";
                }

                if (tag == FROM)
                {
                    di::as<IType>::yield<FROM>();
                }
                else
                {
                    return range<FROM + 1, TO>::yield(tag);
                }
            }
        };

        /**
         * @brief specialization of range struct as termination of recursive search
         *
         * @tparam FROM
         */
        template<int FROM>
        struct range<FROM, FROM>
        {
            constexpr static IType* yield(int tag)
            {
                throw "object not found";
            }
        };

        template<int FROM>
        struct from
        {
            template<int TO>
            struct to
            {
                constexpr static IType* yield(int tag)
                {
                    return range<FROM, TO+1>::yield(tag);
                }
            };
        };
    };
}
